" brings in plugins
call plug#begin('~/.local/share/nvim/:plugged')
Plug 'w0rp/ale'
Plug 'iCyMind/NeoSolarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/vim-easy-align'
call plug#end()

colorscheme NeoSolarized
set background=dark
let g:airline_solarized_bg='dark'
let g:airline#extensions#ale#enabled = 1

" show line numbers
set number
