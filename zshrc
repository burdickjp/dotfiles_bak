# Lines configured by zsh-newuser-install
fpath=( "$HOME/.zfunctions" $fpath )

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -v

autoload -Uz compinit promptinit
compinit
promptinit

prompt pure

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# this provides a nice color output for the ls command
alias ls='pwd; ls --color -l'

# this fixes colors in tmux
alias tmux='tmux -2'
